#Fizzbuzz buzz buzz

##Approach
The original is pretty straight forward. The only thing to note about that is
originally I wanted to use case, when, but every syntax I tried was slightly off
so I went with the easier if conditionals.

The extensible one was a bit harder. After breaking down the reqs, here's what
I felt it should do:
-have the list of number => actions from the original
-be able to add new actions for any number
-use any given range that the user wants, and print out all the crazy actions

The easiest way I felt was to create a class - plus it lets the user have multiple
fizzbuzz logics available. At first I just had the hash in willy nilly, but didn't
give the user a way to initialize the class, so later I added that method and threw
the standard num => act in it.

Next, I made a method to add any other num => act into the hash.

Lastly - and I went through a few different versions of this from my original sudo
code - I needed a way to allow the user to input a range, run through that range, and
print out anything that matched up with their set up logic. So, that meant a method
that takes a low and high number. It meant that this method would run through every
integer in that range. It meant that it would run through every num in the hash,
check if it was a divisor, and if it was add the action to a string to print out.

I figured this would be a nested iteration, which is O(n^2). I'd like that to be
better, but I'd also like to turn in my code, so I went with this structure.
After a few tries and correcting bad syntax, I was able to finish with a pretty
clean extensible program. If I can find some time, there are some parts of the main
method that can be broken into smaller methods to clean up the code.

