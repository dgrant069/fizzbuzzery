class ShinyFizzbuzzer
  def initialize
    @buzz_actions = {
      3 => "fizz",
      5 => "buzz",
      7 => "sivv"
    }
  end

  def add_buzz_action(num, action)
    @buzz_actions[num] = action
  end

  def buzzer(bottomRange, topRange)
    for i in bottomRange.to_i..topRange.to_i
      printable = ""
      @buzz_actions.each do |key, value|
        if i % key == 0
          printable += value
        end
      end
      if printable == ""
        puts i
      else
        puts printable
      end
    end
  end
end
